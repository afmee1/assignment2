<!DOCTYPE html>
<html>
<head>

    <script src="./jquery1.js"></script>
    <link rel="stylesheet" type="text/css" href="Table-Modifier.css" />
</head>
<body>
<?php
include("connection.php");
$conn= mysqli_connect($Host, $UName, $PWord, $DB)
or die("Could not log on to database");
$query="SELECT product_id, product_name,product_sale_price, product_purchase_price, product_country_of_origin FROM product";
$result= mysqli_query($conn, $query); ?>
<form >
    <header>Product List</header>
    <table>
        <thead>
        <tr>
            <th> Name</th>
            <th> Sale Price</th>
            <th> Purchase Price</th>
            <th> Country of Origin</th>
            <th> Action</th>

        </tr>
        </thead>
        <tbody>
        <?php
        while ($row=mysqli_fetch_array($result))
        {
            ?>

            <tr>
                <td><?php echo $row["product_name"] ?></td>
                <td><?php echo $row["product_sale_price"] ?></td>
                <td><?php echo $row["product_purchase_price"] ?></td>
                <td><?php echo $row["product_country_of_origin"] ?></td>
                <td><a href="prodModify.php?product_id= <?php echo $row["product_id"]; ?> &Action=Update">Update</a> <a href="prodModify.php?product_id=<?php echo $row["product_id"]; ?>&Action=Delete">Delete</a></td>
            </tr>

            <?php
        }
        ?>

        </tbody>
    </table>
</form>
<!--<center><input type="button" value="Add Product" onclick="window.location='add.php?Action=add'"></center>-->
<!--</body>-->

<script> $(document).ready(function(){
        $('tbody tr:even').addClass('even');
    });
</script>
</html>