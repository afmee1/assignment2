<?php
ob_start();
?>

<html>
<head><title>Product Modification</title></head>
<body>
<script language="JavaScript">
    function confirm_delete()
    {
        window.location='prodModify.php?product_id=<?php echo $_GET["product_id"];?>&Action=ConfirmDelete';

    }
</script>
<?php
include("connection.php");
$conn= mysqli_connect($Host, $UName, $PWord, $DB)
or die("Could not log on to database");
$query1= "SELECT* FROM product WHERE product_id = ".$_GET["product_id"];
$result1= $conn->query(($query1));
$row = $result1->fetch_assoc();
$query2= "SELECT(category_id) FROM product_category WHERE product_id = ".$_GET["product_id"];
$result2= $conn->query(($query2));
$catCheck=$result2->fetch_assoc();
$query3= "SELECT* FROM category ";
$result3= $conn->query(($query3));
$strAction=$_GET["Action"];
switch($strAction) {
case "Update":

    ?>
    <form method="post"
          action="prodModify.php?product_id=<?php echo $_GET["product_id"]; ?>&Action=ConfirmUpdate">
        <center>Product details amendment <br/></center>

        <table align="center" cellpadding="3">

            <tr>
                <td><b>Name</b></td>
                <td><input type="text" name="prodName" size="30" value="<?php echo $row["product_name"]; ?>"</td>
            </tr>
            <tr>
                <td><b>Country of Origin </b></td>
                <td><input type="text" name="prodCountry" size="30" value="<?php echo $row["product_country_of_origin"]; ?>"</td>
            </tr>
            <tr>
                <td><b>Purchase Price </b></td>
                <td><input type="text" name="prodPurchasePrice" size="40" value="<?php echo $row["product_purchase_price"]; ?>"</td>
            </tr>
            <tr>
                <td><b>Sale Price</b></td>
                <td><input type="text" name="prodSalePrice" size="10" value="<?php echo $row["product_sale_price"]; ?>">
                </td>
            </tr>


        </table>
        <br/>
        <table align="center" cellpadding="3" border="3">
            <thead>
            <tr>
                <th style="background-color: #00b3ff">Category</th>
                <th style="background-color: #00b3ff"></th>
            </tr>
            </thead>
            <tbody>
            <?php
            while ($cat=mysqli_fetch_array($result3))
            {
                ?>

                <tr>

                    <td><?php echo $cat["category_name"] ?></td>

                    <td align="center"><input type="checkbox" name="check[]" value="<?php echo $cat["category_id"];?>" <?php if(count(array_intersect($cat["category_id"],$catCheck))>0){echo 'checked';} ?>   </td>
                </tr>

                <?php
            }

            ?>


            </tbody>
        </table>

        <table align="center">
            <tr>
                <td><input type="submit" value="Update Product"></td>
                <td><input type="button" value="Return to List" onclick="window.location='productDisp.php'">
                </td>
            </tr>
        </table>
    </form>
    <?php
    break;

case "ConfirmUpdate":
{

    $query="UPDATE product set product_name='$_POST[prodName]', product_country_of_origin='$_POST[prodCountry]', product_sale_price='$_POST[prodSalePrice]', product_purchase_price='$_POST[prodPurchasePrice]'
                    WHERE product_id=".$_GET["product_id"];
    $result= $conn->query(($query));
    header("Location: productDisp.php");
}
    break;

case "Delete":
    ?>

    <center>Confirm deletion of the following product <br /></center>
    <table align="center" cellpadding="3">
        <tr>
            <td><b>Product ID</b></td>
            <td><?php echo $row["product_id"]; ?></td>
        </tr>
        <tr>
            <td><b>Name</b></td>
            <td><?php echo $row["product_name"]?></td>
        </tr>
        <tr>
            <td><b>Purchase Price</b></td>
            <td><?php echo $row["product_purchase_price"]?></td>
        </tr>
        <tr>
            <td><b>Sale Price</b></td>
            <td><?php echo $row["product_sale_price"]?></td>
        </tr>
        <tr>
            <td><b>Country of Origin</b></td>
            <td><?php echo $row["product_country_of_origin"]?></td>
        </tr>
    </table>
    <br />
    <table align="center">
        <tr>
            <td><input type="button" value="Confirm" onclick="confirm_delete();"></td>
            <td><input type="button" value="Cancel" onclick="window.location='productDisp.php'"></td>
        </tr>
    </table>
    <?php
    break;
case "ConfirmDelete":
$query="DELETE FROM product WHERE product_id=".$_GET["product_id"];
if ($conn->query($query))
{
?>
<center>
    The following Product has been deleted from the system<p/>
    <?php
    echo "Product $row[product_id] $row[product_name] ";
    echo "</center><p />";
    }
    else{
        echo "<center> Error deleteing product <p /></center>";
    }
    echo "<center><input type='button' value='Return to List' onclick='window.location=\"productDisp.php\"'></center> ";
    break;



    }
    $result1->free_result();
    $result2->free_result();
    $result3->free_result();
    $conn->close();
    ?>
</body>
</html>
